// Utility to extract specific sequences from a fastq file if it contains the desired index

package main

import (
    "bufio"
    "flag"
    "fmt"
    "compress/gzip"
    "log"
    "os"

    "bitbucket.org/GottySG36/sequtils"
)

var (
    synopsis    = `Utility to read in fastq files and trimm lengths according to an input parameter`
    uString     = `trim-fastq-lengths -r1 <PATH/TO/R1> -r2 <PATH/TO/R2> -l <INT>
                           -p1 <PREFIX1> -p2 <PREFIX2> [-z]`
    r1      = flag.String("r1", "", "R1 fragment or single sequence to trim from end to start.")
    r2      = flag.String("r2", "", "R2 fragment or single sequence to trim from start to end.")
    length  = flag.Int("l", -1, "Length of sequence to keep.")
    out1   = flag.String("o1", "", "Output prefix to use for R1 fragment.")
    out2   = flag.String("o2", "", "Output prefix to use for R2 fragment.")
    zip     = flag.Bool("z", false, "Specifies that the output result is to be gzip-compressed.")

    details     = `This tool reads in one or two two files depending on what is required. Each file
is loaded in memory and then all sequences are cut using the specified length.

Depending on which fragment is given, the sequence will eihter be truncated at
the end of the sequence (R1), or at the beginning (R2).

The output result is then written to file '-o1' and/or '-o2', respectively. By
default, the output will not be compressed, but if using '-z', we use gzip to
write the result.
`
)

var Usage = func() {
    fmt.Fprintf(flag.CommandLine.Output(), "%v\n\n", synopsis)
    fmt.Fprintf(flag.CommandLine.Output(), "Usage : %v\n\n", uString)

    fmt.Fprintf(flag.CommandLine.Output(), "Arguments of %s:\n", os.Args[0])
    flag.PrintDefaults()

    fmt.Printf("\nDetailed description :\n%v\n", details)
}

func TrimFq(fsq sequtils.Fsq, l int) (error) {
    var err error
    for idx, seq := range fsq {
        if len(seq.Sequence) <= l {
            continue
        }
        fsq[idx], err = seq.Subsequence(0, l-1)
        if err != nil {
            return err
        }
    }
    return nil
}

// Reads fastq file fq, n lines at a time and trims sequences to length l.
// Output is written to "p".fastq[.gz]
func ProcessFq(fq, pref string, n, l int, zip bool) {
    var eof bool
    var err error
    var w *bufio.Writer
    var wg *gzip.Writer
    var of *os.File

    r, f, err := sequtils.OpenFastq(fq)
    if err != nil {
        log.Fatalf("Error -:- OpenFastq : %v\n", err)
    }
    defer f.Close()

    outp := pref
    if pref != "-" {
        outp = fmt.Sprintf("%v.fastq", pref)
        if zip {
            outp = fmt.Sprintf("%v.fastq.gz", outp)
        }
    }
    if zip {
        wg, of, err = sequtils.CreateFileGzip(outp)
        if err != nil {
            log.Fatalf("ERROR -:- CreateFileGzip : %v\n", err)
        }
    } else {
        w, of, err = sequtils.CreateFile(outp)
        if err != nil {
            log.Fatalf("ERROR -:- CreateFile : %v\n", err)
        }
    }
    defer of.Close()
    defer func(z bool) {
        if zip {
            wg.Flush()
            wg.Close()
        } else {
            w.Flush()
        }
    }(zip)

    for {
        if eof {
            break
        }
        fq, eof, err := sequtils.LoadNFastq(n, r)
        if err != nil {
            log.Fatalf("ERROR -:- LoadNFastq : %v\n", err)
        }

        err = TrimFq(fq, l)
        if err != nil {
            log.Fatalf("Error -:- TrimAllR1 : %v\n", err)
        }

        if zip {
            err = fq.WriteAppendGzip(wg)
            if err != nil {
                log.Fatalf("ERROR -:- WriteAppendGzip : %v\n", err)
            }
        } else {
            err = fq.WriteAppend(w)
            if err != nil {
                log.Fatalf("ERROR -:- WriteAppend : %v\n", err)
            }
        }
        if eof {
            break
        }
    }

}

func main() {
    flag.Usage = Usage
    flag.Parse()

    if *r1 == "" && *r2 == "" {
        log.Fatalf("Error -:- No input provided. You must provide at least one file using '-r1' or/and '-r2'.\n")
    }

    if (*r1 != "" && *out1 == ""){
        log.Fatalf("Error -:- R1 provided but no corresponding output file path given. Use '-o1' to specify one.\n")
    }

    if (*r2 != "" && *out2 == ""){
        log.Fatalf("Error -:- R2 provided but no corresponding output file path given. Use '-o2' to specify one.\n")
    }

    if *length <= 0 {
        log.Fatalf("Error -:- Trimming length must be a number greater than zero (0). Specify it using '-l' option.\n")
    }

    if *r1 != "" {
        ProcessFq(*r1, *out1, 1000000, *length, *zip)
    }
    if *r2 != "" {
        ProcessFq(*r2, *out2, 1000000, *length, *zip)
    }
}
